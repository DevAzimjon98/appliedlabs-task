package me.demo.yandexsimulator.domain.model

import com.google.android.gms.maps.model.LatLng

data class LocationPoint(
    val placeId: String,
    val latLng: LatLng,
    val formattedAddress: String,
    val name: String = "",
)