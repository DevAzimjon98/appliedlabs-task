package me.demo.yandexsimulator.data.database.dao

import androidx.room.*
import kotlinx.coroutines.flow.Flow
import me.demo.yandexsimulator.data.database.model.FavoriteAddressEntity

@Dao
interface FavoriteAddressDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(bookPage: FavoriteAddressEntity)

    @Delete
    fun delete(bookPage: FavoriteAddressEntity)

    @Query("SELECT * FROM favorite_addresses")
    fun getAllBookmarks(): Flow<List<FavoriteAddressEntity>>
}